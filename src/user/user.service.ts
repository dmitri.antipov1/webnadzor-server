import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/createUser.dto';
import { UserEntity } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { sign } from 'jsonwebtoken'
import { JWT_SECRET } from 'src/config';
import { UserResponseInterface } from './types/userResponse.interface';
import { LoginUserDto } from './dto/login.dto';
import { compare } from 'bcrypt';
import { UpdateUserDto } from './dto/updateUser.dto';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>
    ) {}

    async createUser(createUserDto: CreateUserDto): Promise<UserEntity> {
        const userByEmail = await this.userRepository.findOne({ email: createUserDto.email });
        const userByUsername = await this.userRepository.findOne({ username: createUserDto.username });
        if (userByEmail || userByUsername) {
            throw new HttpException('Email or username are taken', HttpStatus.UNPROCESSABLE_ENTITY);
        }
        const newUser = new UserEntity();
        console.log(newUser, 'New user')
        Object.assign(newUser, createUserDto)
        return await this.userRepository.save(newUser);
    }

    async login(loginUserDto: LoginUserDto): Promise<UserEntity> {
        const userByEmail = await this.userRepository.findOne(
            { email: loginUserDto.email },
            { select: ['id', 'username', 'email', 'bio', 'image', 'password', 'position'] }
        );

        if (!userByEmail) {
            throw new HttpException('Credentials are not valid', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        const isPasswordCorrect = await compare(loginUserDto.password, userByEmail.password);

        if (!isPasswordCorrect) {
            throw new HttpException('Credentials are not valid', HttpStatus.UNPROCESSABLE_ENTITY)
        }

        delete userByEmail.password;

        return userByEmail;
    }

    findById(id: number): Promise<UserEntity> {
        return this.userRepository.findOne(id);
    }

    generateJwt(user: UserEntity): string {
        return sign({
            id: user.id,
            username: user.username,
            email: user.email
        }, JWT_SECRET);
    }

    buildUserResponse(user: UserEntity): UserResponseInterface {
        return {
            user: {...user, token: this.generateJwt(user)}
        }
    }

    async updateUser(currentUserId: number, updateUserDto: UpdateUserDto): Promise<UserEntity> {
        const user = await this.findById(currentUserId);
        Object.assign(user, updateUserDto);
        return await this.userRepository.save(user);
    }
}
