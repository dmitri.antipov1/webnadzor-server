import { Body, Controller, Post } from '@nestjs/common';
import { EmailService } from './email.service';
import { EmailBodyInterface } from './types/emailBody.interface';

@Controller('email')
export class EmailController {
    constructor(private readonly mailService: EmailService) {}

    @Post()
    async sendEmail(@Body() body: EmailBodyInterface) {
        return await this.mailService.sendMail(body);
    }
}
