export interface EmailBodyInterface {
    email: string,
    name: string,
    phone: string,
    message: string
}
