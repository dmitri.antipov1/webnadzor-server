export interface CalculatorInterface {
    organization: string;
    isRegistration: string;
    personal: string;
    equipment: string;
    email: string;
    phone: string;
}
