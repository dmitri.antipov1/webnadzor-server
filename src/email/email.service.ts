import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { EmailBodyInterface } from './types/emailBody.interface';

@Injectable()
export class EmailService {
    constructor(private mailerService: MailerService) {}

    async sendMail(body: EmailBodyInterface) {
        await this.mailerService.sendMail({
            to: 'webnadzor.russia@gmail.com',
            from: body.email,
            subject: 'Вам пришло новое сообщение',
            template: 'template',
            context: {
                body: body
            }
        })
    }
}
