import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ReviewsEntity } from './reviews.entity';
import { ReviewsService } from './reviews.service';
import { ReviewsResponseInterface } from './types/reviewsResponse.interface';
import { CreateReviewDto } from './dto/createReview.dto';
import { AuthGuard } from '../user/guards/auth.guard';

@Controller('reviews')
export class ReviewsController {
    constructor(private readonly rc: ReviewsService) {}

    @Get()
    async getAllReviews(): Promise<ReviewsEntity[]> {
        return this.rc.getAllReviews();
    }

    @Get('enabled')
    async getEnabledReviews(): Promise<ReviewsEntity[]> {
        return this.rc.getEnabledReviews();
    }

    @Post('create')
    @UsePipes(new ValidationPipe())
    async createReview(
        @Body() createReviewDto: CreateReviewDto
    ): Promise<ReviewsResponseInterface> {
        const review = await this.rc.createReview(createReviewDto)
        return this.rc.buildReviewResponse(review);
    }

    @Get(':id')
    @UseGuards(AuthGuard)
    @UsePipes(new ValidationPipe())
    async getSingleReview(
        @Param('id') reviewId: number
    ): Promise<ReviewsEntity> {
        return this.rc.getSingleReview(reviewId);
    }

    @Put(':id')
    @UseGuards(AuthGuard)
    @UsePipes(new ValidationPipe())
    async updateSingleReview(
        @Param('id') reviewId: number,
        @Body() createReviewDto: CreateReviewDto
    ): Promise<ReviewsEntity> {
        return this.rc.updateSingleReview(reviewId, createReviewDto);
    }

    @Delete(':id')
    @UseGuards(AuthGuard)
    async deleteSingleReview(
        @Param('id') reviewId: number
    ) {
        return this.rc.deleteSingleReview(reviewId);
    }

}
