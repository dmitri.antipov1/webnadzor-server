import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReviewsController } from './reviews.controller';
import { ReviewsService } from './reviews.service';
import { ReviewsEntity } from './reviews.entity';

@Module({
    controllers: [ReviewsController],
    providers: [ReviewsService],
    imports: [TypeOrmModule.forFeature([ReviewsEntity])]
})
export class ReviewsModule {}
