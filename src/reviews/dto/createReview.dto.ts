import { IsEmail, IsEmpty, IsNotEmpty } from 'class-validator';

export class CreateReviewDto {
    @IsNotEmpty()
    readonly author: string;

    @IsNotEmpty()
    readonly organization: string;

    @IsNotEmpty()
    readonly review: string;

    readonly isVisible: boolean;
}
