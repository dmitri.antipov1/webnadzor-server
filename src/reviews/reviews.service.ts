import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { ReviewsEntity } from './reviews.entity';
import { CreateReviewDto } from './dto/createReview.dto';
import { ReviewsResponseInterface } from './types/reviewsResponse.interface';

@Injectable()
export class ReviewsService {
    @InjectRepository(ReviewsEntity)
    private readonly reviewsRepository: Repository<ReviewsEntity>

    public getAllReviews(): Promise<ReviewsEntity[]> {
        return this.reviewsRepository.find({
            order: { createdAt: "DESC" }
        });
    }

    public getEnabledReviews(): Promise<ReviewsEntity[]> {
        return this.reviewsRepository.find({
            where: { isVisible: true },
            order: { createdAt: "DESC" }
        });
    }

    async createReview(createReviewDto: CreateReviewDto): Promise<ReviewsEntity> {
        const review = new ReviewsEntity();
        Object.assign(review, createReviewDto)

        return await this.reviewsRepository.save(review);
    }

    async getSingleReview(reviewId: number): Promise<ReviewsEntity> {
        const review = await this.reviewsRepository.findOne({ id: reviewId });

        if (!review) {
            throw new HttpException('Review does not exist', HttpStatus.NOT_FOUND);
        }

        return this.reviewsRepository.findOne({ id: reviewId });
    }

    async updateSingleReview(
        reviewId: number,
        updateReviewDto: CreateReviewDto
    ): Promise<ReviewsEntity> {
        const review = await this.reviewsRepository.findOne({ id: reviewId });

        if (!review) {
            throw new HttpException('Review does not exist', HttpStatus.NOT_FOUND);
        }

        Object.assign(review, updateReviewDto)

        return await this.reviewsRepository.save(review);
    }

    async deleteSingleReview(reviewId: number): Promise<DeleteResult> {
        const review = await this.reviewsRepository.findOne({ id: reviewId })

        if(!review) {
            throw new HttpException('Review does not exist', HttpStatus.NOT_FOUND);
        }

        return await this.reviewsRepository.delete({id: reviewId});
    }


    public buildReviewResponse(review: ReviewsEntity): ReviewsResponseInterface {
        return {reviews: review}
    }
}
