import { BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({name: 'reviews'})
export class ReviewsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    author: string;

    @Column()
    organization: string;

    @Column()
    review: string;

    @Column({default: false})
    isVisible: boolean;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    createdAt: Date;

    @BeforeUpdate()
    updateTimestamp() {
        this.updatedAt = new Date()
    }

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'})
    updatedAt: Date;
}
