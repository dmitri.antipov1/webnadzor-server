import { ConfigService } from '@nestjs/config';
import { ormConfig } from './ormconfig';

const configEntity = new ConfigService();

const ormSeedConfig = async (cs) => ({
    ... await ormConfig(cs),
    migrations: ['src/seeds/*.ts'],
    cli: {
        migrationsDir: 'src/seeds',
    },
});

export default ormSeedConfig(configEntity);
