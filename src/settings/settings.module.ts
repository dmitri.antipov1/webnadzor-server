import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SettingsEntity } from './settings.entity';
import { SettingsController } from './settings.controller';
import { SettingsService } from './settings.service';

@Module({
    controllers: [SettingsController],
    providers: [SettingsService],
    imports: [TypeOrmModule.forFeature([SettingsEntity])]
})
export class SettingsModule {}
