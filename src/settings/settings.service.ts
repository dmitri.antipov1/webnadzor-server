import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SettingsEntity } from './settings.entity';
import { Repository } from 'typeorm';
import { SettingsResponseInterface } from './types/settingsResponse.interface';
import { UpdateSettingsDto } from './dto/updateSettings.dto';

@Injectable()
export class SettingsService {
    constructor(
        @InjectRepository(SettingsEntity)
        private readonly settingsRepository: Repository<SettingsEntity>
    ) {}

    public async getSettings(): Promise<SettingsEntity> {
        return this.settingsRepository.findOne({id: 1});
    }

    public buildSettingsResponse(settings: SettingsEntity): SettingsResponseInterface {
        return {settings};
    }

    async updateSettings(id: number, updateSettingsDTO: UpdateSettingsDto): Promise<SettingsEntity> {
        const settings = await this.settingsRepository.findOne({id});

        if(!settings) {
            throw new HttpException('Settings does not exist', HttpStatus.NOT_FOUND);
        }

        Object.assign(settings, updateSettingsDTO);

        return await this.settingsRepository.save(settings);
    }
}
