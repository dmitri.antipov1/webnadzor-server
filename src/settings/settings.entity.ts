import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('settings')
export class SettingsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    tgLink: string;

    @Column()
    phone: string;

    @Column()
    email: string;

    @Column()
    ytLink: string;

    @Column()
    wsLink: string;

    @Column()
    address: string;
}
