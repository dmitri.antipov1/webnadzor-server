import { Body, Controller, Get, Param, Put, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { SettingsService } from './settings.service';
import { SettingsResponseInterface } from './types/settingsResponse.interface';
import { UpdateSettingsDto } from './dto/updateSettings.dto';
import { AuthGuard } from '../user/guards/auth.guard';

@Controller('settings')
export class SettingsController {
    constructor(private readonly ss: SettingsService) {}

    @Get()
    async getAllSettings(): Promise<SettingsResponseInterface> {
        const settings = await this.ss.getSettings();
        return this.ss.buildSettingsResponse(settings)
    }

    @Put(':id')
    @UseGuards(AuthGuard)
    @UsePipes(new ValidationPipe())
    async updateSettings(
        @Param('id') id: number,
        @Body() updateSettingsDTO: UpdateSettingsDto
    ): Promise<SettingsResponseInterface> {
        const updatedSettings = await this.ss.updateSettings(id, updateSettingsDTO);
        return this.ss.buildSettingsResponse(updatedSettings);
    }
}
