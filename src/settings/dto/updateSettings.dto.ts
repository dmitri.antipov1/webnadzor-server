import { IsNotEmpty } from 'class-validator';

export class UpdateSettingsDto {
    @IsNotEmpty()
    tgLink: string;

    @IsNotEmpty()
    phone: string;

    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    ytLink: string;

    @IsNotEmpty()
    address: string;

    @IsNotEmpty()
    wsLink: string;
}
