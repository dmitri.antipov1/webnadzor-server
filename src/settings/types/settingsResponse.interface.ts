import { SettingsEntity } from '../settings.entity';

export interface SettingsResponseInterface {
    settings: SettingsEntity;
}
