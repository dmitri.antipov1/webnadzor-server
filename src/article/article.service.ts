import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserEntity } from '../user/user.entity';
import { CreateArticleDto } from './dto/createArticle.dto';
import { ArticleEntity } from './article.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { ArticleResponseInterface } from './types/articleResponse.interface';
import slugify from 'slugify';

@Injectable()
export class ArticleService {
    constructor(
        @InjectRepository(ArticleEntity) private readonly articleRepository: Repository<ArticleEntity>,
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>
    ) {}

    async createArticle(
        currentUser: UserEntity,
        articleDto: CreateArticleDto
    ): Promise<ArticleEntity> {
        const article = new ArticleEntity();
        Object.assign(article, articleDto)

        article.slug = this.getSlug(articleDto.title);

        article.author = currentUser;

        return await this.articleRepository.save(article);
    }

    async findBySlug(slug: string): Promise<ArticleEntity> {
        return await this.articleRepository.findOne({slug});
    }

    buildArticleResponse(article: ArticleEntity): ArticleResponseInterface {
        return { article }
    }

    private getSlug(title: string): string {
        return slugify(title, { lower: true }) + '_' + ((Math.random() * Math.pow(36, 6)) | 0).toString(36);
    }

    async deleteArticle(slug: string, currentUserId: number): Promise<DeleteResult> {
        const article = await this.findBySlug(slug);

        if(!article) {
            throw new HttpException('Article does not exist', HttpStatus.NOT_FOUND);
        }

        if(article.author.id !== currentUserId) {
            throw new HttpException('You are not an author', HttpStatus.NOT_FOUND);
        }

        return await this.articleRepository.delete({slug});
    }

    async updateArticle(
        slug: string,
        updateArticleDto: CreateArticleDto,
        currentUserId: number
    ): Promise<ArticleEntity> {
        const article = await this.findBySlug(slug);

        if(!article) {
            throw new HttpException('Article does not exist', HttpStatus.NOT_FOUND);
        }

        if(article.author.id !== currentUserId) {
            throw new HttpException('You are not an author', HttpStatus.NOT_FOUND);
        }

        Object.assign(article, updateArticleDto);

        return await this.articleRepository.save(article);
    }

    public getAllArticles() {
        return this.articleRepository.find({
            order: {createdAt: "ASC"}
        });
    }
}
