import { MigrationInterface, QueryRunner } from "typeorm";

export class SeedDB1675524766414 implements MigrationInterface {
    name = 'SeedDB1675524766414'

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.query(
            `INSERT INTO settings ("tgLink", "phone", "email", "ytLink", "address", "wsLink")
             VALUES ('https://t.me/webnadzor', '+7(495)740-37-97', 'webnadzor.russia@gmail.com', 'https://www.youtube.com/@user-vo9cn3po9h', 'г. Москва, ул. Усачева, д. 15А', '+79260673797')`,
        );

        await queryRunner.query(
            `INSERT INTO users ("username", "email", "password")
             VALUES ('Dmitri Antipov', 'dmitri.antipov1@gmail.com', '$2b$10$R1UU4wuVGObC5jSUwNz1F./3n/94B4.XAvJ2Z0xxKPrYee4IYNqRO'),
                     ('Webnadzor Admin', 'webnadzor.russia@gmail.com', '$2b$10$vOJzdGMe6P7vxav17pLBUe8QpdNRc23aFtcDkQfhk1esgv06L2H2e')`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
