import { ConfigService } from '@nestjs/config';
import { join } from 'path';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

export const emailConfig = async (cs: ConfigService) => ({
    transport: {
        host: cs.get('EMAIL_HOST'),
        port: cs.get('EMAIL_PORT'),
        ignoreTLS: true,
        auth: {
            user: cs.get('EMAIL_USER'),
            pass: cs.get('EMAIL_PASSWORD'),
        },
    },
    template: {
        dir: join(__dirname, 'mails'),
        adapter: new HandlebarsAdapter(),
        options: {
            strict: true,
        },
    },
})

