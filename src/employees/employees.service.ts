import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EmployeesEntity } from './employees.entity';
import { DeleteResult, Repository } from 'typeorm';
import { EmployeesDto } from './dto/employees.dto';

@Injectable()
export class EmployeesService {
    constructor(
        @InjectRepository(EmployeesEntity)
        private readonly employeeRepository: Repository<EmployeesEntity>
    ) {
    }

    async getAllEmployees(): Promise<EmployeesEntity[]> {
        return await this.employeeRepository.find();
    }

    async createEmployee(employeeDTO: EmployeesDto): Promise<EmployeesEntity> {
        const employee = new EmployeesEntity();
        Object.assign(employee, employeeDTO);
        return await this.employeeRepository.save(employee);
    }

    async updateEmployee(id: number, employeeDTO: EmployeesDto): Promise<EmployeesEntity> {
        const employee = await this.employeeRepository.findOne({id});
        if (!employee) {
            throw new HttpException('Employee does not exist', HttpStatus.NOT_FOUND);
        }
        Object.assign(employee, employeeDTO)
        return await this.employeeRepository.save(employee);
    }

    public async deleteEmployee(id: number): Promise<DeleteResult> {
        const employee = await this.employeeRepository.findOne({id});
        if (!employee) {
            throw new HttpException('Employee does not exist', HttpStatus.NOT_FOUND);
        }
        return await this.employeeRepository.delete(employee);
    }

    public async getSingleEmployees(id: number): Promise<EmployeesEntity> {
        const employee = await this.employeeRepository.findOne({id});
        if (!employee) {
            throw new HttpException('Employee does not exist', HttpStatus.NOT_FOUND);
        }
        return await employee;
    }
}
