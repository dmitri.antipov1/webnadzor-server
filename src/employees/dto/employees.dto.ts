import { IsNotEmpty } from 'class-validator';

export class EmployeesDto {
    @IsNotEmpty()
    username: string;
    @IsNotEmpty()
    email: string;
    @IsNotEmpty()
    bio: string;
    @IsNotEmpty()
    position: string;
    @IsNotEmpty()
    image: string;
}
