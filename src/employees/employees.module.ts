import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmployeesController } from './employees.controller';
import { EmployeesService } from './employees.service';
import { EmployeesEntity } from './employees.entity';

@Module({
    controllers: [EmployeesController],
    providers: [EmployeesService],
    imports: [TypeOrmModule.forFeature([EmployeesEntity])]
})
export class EmployeesModule {}
