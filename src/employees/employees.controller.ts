import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { EmployeesInterface } from './types/employees.interface';
import { EmployeesService } from './employees.service';
import { EmployeesDto } from './dto/employees.dto';
import { AuthGuard } from '../user/guards/auth.guard';
import { DeleteResult } from 'typeorm';

@Controller('employees')
export class EmployeesController {
    constructor(private readonly es: EmployeesService) {}

    @Get()
    async getAllEmployees(): Promise<EmployeesInterface[]> {
        return await this.es.getAllEmployees();
    }

    @Get(':id')
    async getSingleEmployees(
        @Param('id') id: number,
    ): Promise<EmployeesInterface> {
        return await this.es.getSingleEmployees(id);
    }

    @Post()
    @UseGuards(AuthGuard)
    @UsePipes(new ValidationPipe())
    async createEmployee(
        @Body() employeeDTO: EmployeesDto,
    ): Promise<EmployeesInterface> {
        return await this.es.createEmployee(employeeDTO);
    }

    @Put(':id')
    @UseGuards(AuthGuard)
    @UsePipes(new ValidationPipe())
    async updateEmployee(
        @Param('id') id: number,
        @Body() employeeDTO: EmployeesDto,
    ): Promise<EmployeesInterface> {
        return await this.es.updateEmployee(id, employeeDTO);
    }

    @Delete(':id')
    @UseGuards(AuthGuard)
    @UsePipes(new ValidationPipe())
    async deleteEmployee(
        @Param('id') id: number
    ): Promise<DeleteResult> {
        return await this.es.deleteEmployee(id);
    }

}
