import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('employees')
export class EmployeesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    email: string;

    @Column({default: ''})
    bio: string;

    @Column({default: ''})
    position: string;

    @Column({default: ''})
    image: string;

}
