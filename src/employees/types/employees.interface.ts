export interface EmployeesInterface {
    id: number;
    username: string;
    email: string;
    bio: string;
    position: string;
    image: string;
}
