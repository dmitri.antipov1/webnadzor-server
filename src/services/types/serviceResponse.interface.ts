import { ServicesEntity } from '../services.entity';

export interface ServiceResponseInterface {
    services: ServicesEntity[];
}
