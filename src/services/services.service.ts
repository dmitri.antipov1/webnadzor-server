import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ServicesEntity } from './services.entity';
import { DeleteResult, Repository } from 'typeorm';
import { ServiceResponseInterface } from './types/serviceResponse.interface';
import { CreateServiceDto } from './createService.dto';

@Injectable()
export class ServicesService {
    constructor(
        @InjectRepository(ServicesEntity)
        private readonly servicesRepository: Repository<ServicesEntity>
    ) {
    }

    async getAllServices(): Promise<ServicesEntity[]> {
        return await this.servicesRepository.find({
            order: {createdAt: "DESC"}
        });
    }

    async getAllSiteServices(category: string): Promise<ServicesEntity[]> {
        return await this.servicesRepository.find({category});
    }

    async getAllLicenseServices(category: string) {
        return await this.servicesRepository.find({category});
    }

    async getAllAccreditationServices(category: string) {
        return await this.servicesRepository.find({category});
    }

    public buildServicesResponse(services: ServicesEntity[]): ServiceResponseInterface {
        return {services};
    }

    async createService(createServiceDTO: CreateServiceDto): Promise<ServicesEntity> {
        const service = new ServicesEntity();
        Object.assign(service, createServiceDTO);
        return await this.servicesRepository.save(createServiceDTO);
    }

    async getSingleServices(serviceId: number): Promise<ServicesEntity> {
        const service = await this.servicesRepository.findOne(serviceId);

        if (!service) {
            throw new HttpException('Service does not exist', HttpStatus.NOT_FOUND);
        }

        return service;
    }

    public async editService(updateServiceId: number, updateServiceDTO: CreateServiceDto): Promise<ServicesEntity> {
        const service = await this.servicesRepository.findOne(updateServiceId);

        if (!service) {
            throw new HttpException('Service does not exist', HttpStatus.NOT_FOUND);
        }

        Object.assign(service, updateServiceDTO);

        return await this.servicesRepository.save(service);
    }

     async deleteService(deleteServiceId: number): Promise<DeleteResult> {
        const service = await this.servicesRepository.findOne(deleteServiceId);

        if (!service) {
            throw new HttpException('Service does not exist', HttpStatus.NOT_FOUND);
        }

        return await this.servicesRepository.delete({id: service.id});
    }
}
