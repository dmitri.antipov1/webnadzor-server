import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServicesController } from './services.controller';
import { ServicesService } from './services.service';
import { ServicesEntity } from './services.entity';

@Module({
    controllers: [ServicesController],
    providers: [ServicesService],
    imports: [TypeOrmModule.forFeature([ServicesEntity])]
})
export class ServicesModule {}
