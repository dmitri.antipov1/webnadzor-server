import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ServicesService } from './services.service';
import { ServiceResponseInterface } from './types/serviceResponse.interface';
import { ServicesEntity } from './services.entity';
import { CreateServiceDto } from './createService.dto';
import { AuthGuard } from '../user/guards/auth.guard';
import { DeleteResult } from 'typeorm';

@Controller('services')
export class ServicesController {
    constructor(private readonly ss: ServicesService) {
    }

    @Get()
    async getAllServices(): Promise<ServiceResponseInterface> {
        const services = await this.ss.getAllServices();
        return this.ss.buildServicesResponse(services)
    }

    @Get('site')
    async getAllSiteServices(): Promise<ServiceResponseInterface> {
        const services = await this.ss.getAllSiteServices('site');
        return this.ss.buildServicesResponse(services)
    }

    @Get('license')
    async getAllLicenseServices(): Promise<ServiceResponseInterface> {
        const services = await this.ss.getAllLicenseServices('license');
        return this.ss.buildServicesResponse(services)
    }

    @Get('accreditation')
    async getAllAccreditationServices(): Promise<ServiceResponseInterface> {
        const services = await this.ss.getAllAccreditationServices('accreditation');
        return this.ss.buildServicesResponse(services)
    }

    @Get(':id')
    @UseGuards(AuthGuard)
    async getSingleServices(
        @Param('id') serviceId: number
    ): Promise<ServicesEntity> {
        return await this.ss.getSingleServices(serviceId);
    }

    @Post()
    @UseGuards(AuthGuard)
    @UsePipes(new ValidationPipe())
    async addService(
        @Body() createServiceDTO: CreateServiceDto
    ): Promise<ServicesEntity> {
        return await this.ss.createService(createServiceDTO);
    }

    @Put(':id')
    @UseGuards(AuthGuard)
    @UsePipes(new ValidationPipe())
    async editService(
        @Param('id') updateServiceId: number,
        @Body() updateServiceDTO: CreateServiceDto
    ): Promise<ServicesEntity> {
        return await this.ss.editService(updateServiceId, updateServiceDTO);
    }

    @Delete(':id')
    @UseGuards(AuthGuard)
    async deleteService(
        @Param('id') deleteServiceId: number
    ): Promise<DeleteResult> {
        return this.ss.deleteService(deleteServiceId)
    }
}
