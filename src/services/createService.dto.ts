import { IsNotEmpty } from 'class-validator';

export class CreateServiceDto {
    @IsNotEmpty()
    category: string;

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    price: string;
}
