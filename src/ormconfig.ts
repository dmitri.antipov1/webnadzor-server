import { ConfigService } from '@nestjs/config';

const configEntity = new ConfigService();

export const ormConfig = async (cs: ConfigService) => ({
    type: 'postgres',
    host: cs.get('POSTGRES_HOST'),
    port: +cs.get('POSTGRES_PORT'),
    username: cs.get('POSTGRES_USERNAME'),
    password: cs.get('POSTGRES_PASSWORD'),
    database: cs.get('POSTGRES_DATABASE'),
    synchronize: false,
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
    cli: {
        migrationsDir: 'src/migrations',
    },
})

export default ormConfig(configEntity);
